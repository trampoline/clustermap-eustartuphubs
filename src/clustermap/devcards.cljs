(ns clustermap.devcards
  (:require
   [devcards.core :as dc :include-macros true]
   [cljs.core.async :as async]
   [clustermap.components.edit-company :as ec]
   [clustermap.api :as api]
   [devtools.core :as devtools]
   [cljs.pprint :as pprint]
   )
  (:require-macros
   [devcards.core :refer [defcard deftest]]
   [cljs.test :refer [is testing]]))

(devtools/install!)
(enable-console-print!)
(dc/start-devcard-ui!)

#_(defcard intro
    {:h->bid ec/hub->boundaryline-id
     ;; :jsconfig js/config.api
     ;; :company-schema ec/Company
     ;;:record-fact api/records-factory
     :api-prefix  api/api-prefix}
    )

(def initial-company-data
  {:records [{:address "24 a road"
              :tags [{:type "startup_region" :tag "nuts_1__ES3"}]
              :name "A company"
              :direct_contact_email "blah@foo.com"
              :country_code "GB"
              :formation_date "2005-05-30"
              :latest_accounts_date "2005-06-30"
              :latest_turnover 123456
              :natural_id "moo"
              :postcode "abc"
              :location [1 2]
              :angellist_url "http://anglelist"
              :crunchbase_url "http://crunch"
              :dealroom_url "http://dealroom"
              :description "blah blah"
              :latest_employee_count 85}]})

(defn mk-stub-metadata-fn [data]
  (fn [& _]
    (let [ch (async/promise-chan)]
      (async/onto-chan ch [data])
      ch)))

(def error-map
  {:error-code 1
   :response-text "Server Error"
   :status 500})

;;(ex-info "moo" error-map)

(defcard new-company-form
  "Should be blank"
  (dc/om-root ec/edit-company-component
              {:shared {:fetch-metadata-factory #(mk-stub-metadata-fn {})
                        :navigate-fn (constantly true)
                        :submit-company-fn (mk-stub-metadata-fn {:payload "hi"})}})
  {:record nil :metadata {:record nil}
   }
  )

(defcard edit-existing-company
  "Correctly put existing data in the form "
  (dc/om-root ec/edit-company-component
              {:shared {:fetch-metadata-factory
                        #(mk-stub-metadata-fn initial-company-data)
                        :navigate-fn (constantly true)
                        :submit-company-fn (mk-stub-metadata-fn {:payload "hi"})}})
  {:record nil :metadata {:record nil}
   ::initial-company-data initial-company-data}
  {:inspect-data true :history false}
  )

(defcard edit-bad-data
  ""
  (dc/om-root ec/edit-company-component
              {:shared {:fetch-metadata-factory
                        #(mk-stub-metadata-fn (assoc-in initial-company-data
                                                        [:records 0 :natural_id] nil))
                        :navigate-fn (constantly true)
                        :submit-company-fn (mk-stub-metadata-fn {:payload "hi"})}})
  {:record nil :metadata {:record nil}
   ::initial-company-data initial-company-data}

  )

(defcard post-error
  "Testing `Save` button giving a 500 error"
  (dc/om-root ec/edit-company-component
              {:shared {:fetch-metadata-factory
                        #(mk-stub-metadata-fn initial-company-data)
                        :navigate-fn (constantly true)
                        :submit-company-fn (mk-stub-metadata-fn (ex-info "fail" error-map))}})
  {:record nil :metadata {:record nil}
   ::initial-company-data initial-company-data}

  )

(defcard real-post
  "Testing `Save` to running api"
  (dc/om-root ec/edit-company-component
              {:shared {:fetch-metadata-factory
                        #(mk-stub-metadata-fn initial-company-data)
                        :navigate-fn (constantly true)
                        :submit-company-fn (partial ec/submit-company "/eustartuphubs/submit-company")}})
  {:record nil :metadata {:record nil}
   ::initial-company-data initial-company-data})

(deftest cljs-test-integrations
  "## Here are some tests"
  (testing "data transforming"
    (is (= {:description nil, :address nil, :employment nil, :turnover nil, :name nil, :postcode nil,
            :angellist_url nil, :boundaryline_id nil, :registry_id nil, :dealroom_url nil, :country_code nil,
            :latitude nil, :longitude nil
            :direct_contact_email nil, :formation_date nil, :crunchbase_url nil, :accounts_date nil}
           (ec/extract-values {})))
    (is (= "abc123" (-> (ec/extract-values {:postcode "abc123"})
                        :postcode)))
    (is (= "nuts_1__ES3"
           (-> (ec/extract-values {:tags [{:type "no" :tag "wrong"}
                                          {:type "startup_region"
                                           :tag "nuts_1__ES3"}]} )
               :boundaryline_id)))
    #_(is (= ["dabc" "nuts_1__ES3"]
             (-> (ec/extract-values {:natural_id "abc" :tags [{:type "startup_region"
                                                               :tag "nuts_1__ES3"}]} )
                 ((juxt :registry_id :boundaryline_id)))))
    (testing " Date not fixed yet"
      (is (=  #inst "2005-05-30" (-> (ec/extract-values {:formation_date "2005-05-30"}) :formation_date))))
    (is (= "abc" (-> (ec/extract-values {:natural_id  "abc"})
                     :registry_id))))

  )
