(ns clustermap.components.company-info
  (:require-macros
   [cljs.core.async.macros :refer [go]])
  (:require [cljs.core.async :refer [<!]]
            [clojure.string :as str]
            [om.core :as om :include-macros true]
            [om-tools.core :refer-macros [defcomponentk]]
            [schema.core :as s :refer-macros [defschema]]
            [sablono.core :as html :refer-macros [html]]
            [clustermap.util :refer-macros [inspect <?] :refer [display pp]]
            [clustermap.api :as api]
            [clustermap.formats.number :as num :refer [div! *! -! +!]]
            [clustermap.formats.money :as money]
            [clustermap.formats.time :as time]))

(defn render-metadata-row
  [record {:keys [key label render-fn] :or {render-fn identity} :as field}]
  [[:div.tbl-cell label]
   [:div.tbl-cell (some-> (get record key) render-fn)]])

(defn sign-icon
  [n]
  (cond
    (> n 0) [:i.icon-positive]
    (< n 0)  [:i.icon-negative]
    :else nil))

(defn stat-change
  [base change]
  (let [prev (-! base change)]
    (when (and change base (not= 0 prev))
      (let [v (*! 100 (div! change prev))]
        [:div.stat-change (sign-icon v) [:span (num/mixed v) "%"]]))))

(defn render*
  [record {:keys [title-field] :as controls}]
  (.log js/console (pp ["RECORD" record]))
  (html
   [:div.panel-grid-container
    [:div.panel-grid
     [:div.panel-row

      [:div.panel
       [:div.panel-body
        [:div.company-details
         [:ul

          [:li
           [:h4 "EU Startup Hubs id"]
           [:p (:natural_id record)]]
          [:li
           [:h4 "Description"]
           [:p (:description record)]]
          [:li
           [:h4 "Website"
            [:p
             [:a {:href (:web_url record) :target "_blank"} (:web_url record)]]]]]]]]

      [:div.panel
       [:div.panel-body
        [:div.company-address
         [:div.row
          [:div.col-sm-6
           (into
            [:address
             [:h4 "Address"]]
            (for [line (str/split (:address record) #",|\n")]
              [:div line]))]]]]]
      ]

     [:div.panel-row

      [:div.panel
       [:div.panel-body
        [:div.chart-heading
         [:h4.stat-title "Revenue"]
         [:div.stat-amount [:small "€"] (num/mixed (:latest_turnover record))]
         (stat-change (:latest_turnover record) (:latest_turnover_delta record))]]]

      [:div.panel
       [:div.panel-body
        [:div.chart-heading
         [:h4.stat-title "Employment"]
         [:div.stat-amount (num/mixed (:latest_employee_count record))]
         (stat-change (:latest_employee_count record) (:latest_employee_count_delta record))]]]
      ]

     [:div.panel-row

      [:div.panel
       [:div.panel-body
        [:h4.stat-title "Funding"]
        [:div.table-responsive
         [:table.table
          [:thead
           [:tr
            [:th "Total Amount raised (€)"]
            [:th "Participants"]]]
          (let [rounds (:funding_rounds record)
                investor-names (sort (into #{} (for [is (map :investments rounds)
                                                     i is]
                                                 (:investor_name i))))]
            [:tbody
             [:tr
              [:td (num/mixed (:total_funding record))]
              [:td [:table
                    (into [:tbody]
                          (for [p investor-names]
                            [:tr [:td p]]))]]]])]]]]

      [:div.panel
       [:div.panel-body
        [:h4.stat-title "Data sources"]
        [:div.table-responsive
         [:table.table
          (let [sources-by-id (->> (:sources record) (map (fn [s] [(:source_id s) s])) (into {}))]
            (js/console.log (pp ["SOURCES" (:sources record)]))
            (js/console.log (pp ["SOURCES-BY-ID" sources-by-id]))
            (into [:tbody]
                  (for [source-tag (filter #(= "datasource" (:type %)) (:tags record))]
                    [:tr
                     [:td
                      (if-let [source-url (->> (:tag source-tag) (get sources-by-id) :source_url)]
                        [:a {:href source-url :target "_blank"}
                         (:description source-tag)]
                        (:description source-tag))]])))]]]]
      ]



     ]]
   ;; [:div.box.box-first
   ;;  [:header (get record title-field)]
   ;;  [:div.tbl
   ;;   (for [field fields]
   ;;     (into [:div.tbl-row]
   ;;           (render-metadata-row record field)))]]
   ))

(defn company-info-component
  [{{record :record
     {index :index
      index-type :index-type
      sort-spec :sort-spec
      size :size
      :as controls} :controls
      :as metadata} :metadata
      filter-spec :filter-spec
      :as props}
   owner]

  (reify
    om/IDidMount
    (did-mount [_]
      (let [fetch-metadata-factory (om/get-shared owner :fetch-metadata-factory)]
        (assert (fn? fetch-metadata-factory) "Should be a factory function")
        (om/set-state! owner :fetch-metadata-fn (fetch-metadata-factory))))

    om/IRender
    (render [_]
      (render* record controls))

    om/IWillUpdate
    (will-update [_
                  {{next-record :record
                    {next-index :index
                     next-index-type :index-type
                     next-sort-spec :sort-spec
                     next-size :size
                     :as next-controls} :controls
                    :as next-metadata} :metadata
                    next-filter-spec :filter-spec}
                  {fetch-metadata-fn :fetch-metadata-fn}]

      (when (or (not next-record)
                (not= next-controls controls)
                (not= next-filter-spec filter-spec))
        (go
          (when-let [metadata-data (<! (fetch-metadata-fn next-index
                                                          next-index-type
                                                          next-filter-spec
                                                          next-sort-spec
                                                          next-size))]
            (.log js/console (pp ["COMPANY-INFO-DATA" metadata-data]))
            (om/update! metadata [:record] (some-> metadata-data :records first))))))))
