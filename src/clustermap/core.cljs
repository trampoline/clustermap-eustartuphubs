(ns clustermap.core
  (:require-macros [hiccups.core :as hiccups]
                   [cljs.core.async.macros :refer [go]])
  (:require
   [clojure.string :as str]
   ;; [weasel.repl :as ws-repl]
   [devtools.core :as devtools]
   [om.core :as om :include-macros true]
   [clustermap.api :as api]
   [clustermap.app :as app]
   [clustermap.filters :as filters]
   [clustermap.formats.time :as time]
   [clustermap.formats.number :as num :refer [div! *! -! +!]]
   [clustermap.formats.money :as money]
   [clustermap.components.map :as map]
   [clustermap.components.filter :as filter]
   [clustermap.components.search :as search]
   [clustermap.components.select-chooser :as select-chooser]
   [clustermap.components.checkbox-boolean :as checkbox-boolean]
   [clustermap.components.color-scale :as color-scale]
   [clustermap.components.map-report :as map-report]
   [clustermap.components.table :as table]
   [clustermap.components.ranges-table :as ranges-table]
   [clustermap.components.ranges-chart :as ranges-chart]
   [clustermap.components.timeline-chart :as timeline-chart]
   [clustermap.components.tag-histogram :as tag-histogram]
   [clustermap.components.geo-sponsors :as geo-sponsors]
   [clustermap.components.filter-component-description :as filter-component-description]
   [clustermap.components.filter-description :as filter-description]
   [clustermap.components.text :as text]
   [clustermap.components.company-info :as company-info]
   [clustermap.components.edit-company :as edit-company]
   [clustermap.components.nav-button :as nav-button]
   [clustermap.components.action-button :as action-button]
   [clustermap.components.action-link :as action-link]
   [clustermap.components.chart-helpers :as chart-helpers]
   [clustermap.boundarylines :as bl]
   [clustermap.util :as util :refer [inspect]]
   [cljs.core.async :refer [chan <! put! sliding-buffer >!]]
   [schema.core :as s :refer-macros [defschema]]))

(def RELEASE "@define {string}" "")
(def RAVEN_DSN "@define {string}" "")

;; assume we are in dev-mode if there is repl support
(def ^:private dev-mode (some-> js/window .-config .-repl))
(when ^boolean js/goog.DEBUG
  (inspect "debug")
  (devtools/install! :all))
(when-not ^boolean js/goog.DEBUG
  (inspect "not debug")
  (inspect (-> (js/Raven.config ^string clustermap.core/RAVEN_DSN
                                #js {:release ^string clustermap.core/RELEASE})
               .install)))

(if ^boolean js/goog.DEBUG (devtools/install!))

;; the IApp object
(def ^:private app-instance (atom nil))

(def brand-secondary "#006DB6")
(def brand-tertiary "#A50F15")

(defn get-app-state-atom
  []
  (-> app-instance
      deref
      clustermap.app/get-state))

(defn boundaryline-filter
  [boundaryline-id]
  (when boundaryline-id
    {:nested {:path "?boundarylines"
              :filter {:term {"boundaryline_id" boundaryline-id}}}}))

(defn make-boundaryline-selection
  [boundaryline-id]
  (let [app-state (app/get-state @app-instance)
        ch (when boundaryline-id
             (bl/get-or-fetch-boundaryline app-state :boundarylines boundaryline-id))]
    (go
      (let [bl (when ch (<! ch))]
        (.log js/console bl)
        (let [bl-filter (when boundaryline-id (boundaryline-filter boundaryline-id))
              bl-name (when boundaryline-id (aget bl "compact_name"))

              updated-filters (filters/update-filter-component (get-in @app-state [:dynamic-filter-spec])
                                                               :boundaryline
                                                               bl-filter
                                                               bl-name
                                                               boundaryline-id)]
          (.log js/console (clj->js updated-filters))

          (swap! app-state
                 assoc-in
                 [:dynamic-filter-spec]
                 updated-filters))))))

(defn make-company-selection
  [natural-id]
  (let [state-atom (get-app-state-atom)
        components (get-in @state-atom [:selection-filter-spec :components])
        components (assoc-in components [:natural-id] {:term {"?natural_id" natural-id}})
        base-filters (get-in @state-atom [:selection-filter-spec :base-filters])
        composed (filters/compose-filters components base-filters)]
    (swap! state-atom update-in [:selection-filter-spec] merge {:components components :composed composed})))

(defn company-link-render-fn
  [name record]
  [:a {:href "#"
       :target "_blank"
       :onClick (fn [e]
                  (some-> e .preventDefault)
                  (make-company-selection (:?natural_id record))
                  (app/navigate @app-instance "company"))}
   name])

(defn edit-company-render-fn
  [name record]
  [:a {:href "#"
       :target "_blank"
       :onClick (fn [e]
                  (some-> e .preventDefault)
                  (make-company-selection (:?natural_id record))
                  (app/navigate @app-instance "edit-company"))}
   name])

(defn edit-company-fn
  [e]
  (.preventDefault e)
  (let [state-atom (get-app-state-atom)
        record (get-in @state-atom [:company-info :record])]
    (make-company-selection (:natural_id record))
    (swap! state-atom assoc-in [:edit-company :controls :new-company] false)
    (swap! state-atom assoc-in [:edit-company :record] nil)
    (app/navigate @app-instance "edit-company")))

(defn edit-new-company-fn
  []
  (let [state-atom (get-app-state-atom)
        record (get-in @state-atom [:company-info :record])]
    (swap! state-atom assoc-in [:edit-company :controls :new-company] true)
    (swap! state-atom assoc-in [:edit-company :record] {})
    (app/navigate @app-instance "edit-company")))


(defn sign-icon
  [n]
  (cond
    (> n 0) [:i.icon-positive]
    (< n 0)  [:i.icon-negative]
    :else nil))

(def initial-state
  {:boundarylines {
                   :collections {
                                 "uk_boroughs" {:index nil
                                                :rtree nil
                                                :boundarylines {}}
                                 "uk_wards" {:index nil
                                             :rtree nil
                                             :boundarylines {}}
                                 "uk_regions" {:index nil
                                               :rtree nil
                                               :boundarylines {}}}
                   :boundarylines {}}

   :dynamic-filter-spec {:id :coll

                         ;; dynamic components
                         :components {:boundaryline nil}

                         ;; dynamic component descriptions
                         :component-descrs {}

                         ;; dynamic component url descriptions
                         :url-components {}

                         ;; filters to compose with components
                         :base-filters {:all nil}

                         ;; specifications for dynamic components
                         :component-specs [
                                           {:id :hub
                                            :type :tag-checkboxes
                                            :label "Hub"
                                            :sorted true
                                            :visible true
                                            :controls true
                                            :tag-type "startup_region"
                                            :tags [{:value "nuts_1__UKI" :label "London"}
                                                   {:value "nuts_1__FR1" :label "Paris"}
                                                   {:value "nuts_2__UKD3" :label "Manchester"}
                                                   {:value "nuts_2__FI1B" :label "Helsinki"}
                                                   {:value "nuts_2__SE11" :label "Stockholm"}
                                                   {:value "nuts_1__BE1" :label "Brussels"}
                                                   {:value "nuts_2__RO32" :label "Bucharest"}
                                                   {:value "nuts_1__ES3" :label "Madrid"}
                                                   {:value "nuts_3__DE212" :label "Munich"}
                                                   {:value "nuts_1__DE3" :label "Berlin"}
                                                   {:value "nuts_3__EE001" :label "Tallinn"}
                                                   {:value "nuts_3__ITI43" :label "Rome"}
                                                   {:value "nuts_2__SE22" :label "Malmo"}
                                                   {:value "nuts_3__NL326" :label "Amsterdam"}
                                                   {:value "nuts_3__EL300" :label "Athens"}
                                                   {:value "nuts_3__PL127" :label "Warsaw"}
                                                   {:value "nuts_3__NO011" :label "Oslo"}
                                                   {:value "nuts_3__IE021" :label "Dublin"}
                                                   {:value "nuts_1__DK0" :label "Copenhagen"}
                                                   {:value "nuts_1__AT1" :label "Vienna"}
                                                   ]}

                                           ;; {:id :boundaryline
                                           ;;  :type :external
                                           ;;  :label "Region"
                                           ;;  :formatter (fn [content] [:h1 content])
                                           ;;  :default-text "All (select from map)"
                                           ;;  :set-filter-for-url (fn [boundaryline-id set-filter]
                                           ;;                        (let [ch (when boundaryline-id
                                           ;;                                   (bl/get-or-fetch-boundaryline (get-app-state-atom) :boundarylines boundaryline-id))]
                                           ;;                          (go
                                           ;;                            (when-let [bl (when ch (<! ch))]
                                           ;;                              (let [bl-filter (when boundaryline-id (boundaryline-filter boundaryline-id))
                                           ;;                                    bl-name (when boundaryline-id (aget bl "compact_name"))]
                                           ;;                                (set-filter bl-filter bl-name))))))}

                                           {:id :age
                                            :type :checkboxes
                                            :label "Age"
                                            :visible true
                                            :controls true
                                            :options [;; {:value "any" :filter nil :label "Any" :omit-description true}
                                                      {:value "new" :label "< 5 years" :filter {:range {"!formation_date" {:gte "2011-01-01"}}}}
                                                      {:value "mid" :label "5 – 10 years" :filter {:range {"!formation_date" {:gte "2006-01-01"
                                                                                                                              :lt "2011-01-01"}}}}
                                                      {:value "old" :label "10 – 15 years" :filter {:range {"!formation_date" {:gte "2001-01-01"
                                                                                                                               :lt "2006-01-01"}}}}
                                                      {:value "oldest" :label "≥ 15 years" :filter {:range {"!formation_date" {:lt "2000-01-01"}}}}]}

                                           {:id :total-funding
                                            :type :checkboxes
                                            :label "Total funding"
                                            :visible true
                                            :controls true
                                            :options [;; {:value "any" :label "Any" :filter nil :omit-description true}
                                                      {:value "min" :label "< €500k" :filter {:range {"!total_funding" {:lt 500000}}}}
                                                      {:value "low" :label "€500k – €5m" :filter {:range {"!total_funding" {:gte 500000 :lt 5000000}}}}
                                                      {:value "lowmid" :label "€5m – €10m" :filter {:range {"!total_funding" {:gte 5000000 :lt 10000000}}}}
                                                      {:value "highmid" :label "€10m – €100m" :filter {:range {"!total_funding" {:gte 10000000 :lt 100000000}}}}
                                                      {:value "high" :label "> €100m" :filter {:range {"!total_funding" {:gte 100000000}}}}
                                                      ]}

                                           ;; {:id :latest-turnover
                                           ;;  :type :checkboxes
                                           ;;  :label "Revenue"
                                           ;;  :options [;; {:value "any" :label "Any" :filter nil :omit-description true}
                                           ;;            {:value "min" :label "< €500k" :filter {:range {"!latest_turnover" {:lt 500000}}}}
                                           ;;            {:value "low" :label "€500k - €5m" :filter {:range {"!latest_turnover" {:gte 500000 :lt 5000000}}}}
                                           ;;            {:value "lowmid" :label "€5m - €10m" :filter {:range {"!latest_turnover" {:gte 5000000 :lt 10000000}}}}
                                           ;;            {:value "highmid" :label "€10m - €100m" :filter {:range {"!latest_turnover" {:gte 10000000 :lt 100000000}}}}
                                           ;;            {:value "high" :label "> €100m" :filter {:range {"!latest_turnover" {:gte 100000000}}}}
                                           ;;            ]}

                                           {:id :sector
                                            :type :checkboxes
                                            :label "Sector"
                                            :visible true
                                            :sorted true
                                            :controls true
                                            :options [;; {:value "any" :label "Any" :filter nil :omit-description true}
                                                      #_{:value "any" :label "Has SIC"
                                                         :filter {:range {"!sic07" {:gte ""}}}}
                                                      {:value "sectionA" :label "Agriculture, forestry & fishing"
                                                       :filter {:range {"!sic07" {:gte "01110" :lt "05101"}}}}
                                                      {:value "sectionB" :label "Mining & quarrying"
                                                       :filter {:range {"!sic07" {:gte "05101" :lt "10110"}}}}
                                                      {:value "sectionC" :label "Manufacturing"
                                                       :filter {:range {"!sic07" {:gte "10110" :lt "35110"}}}}
                                                      {:value "sectionD" :label "Electricity, gas & air conditioning"
                                                       :filter {:range {"!sic07" {:gte "35110" :lt "36000"}}}}
                                                      {:value "sectionE" :label "Water, sewage & waste"
                                                       :filter {:range {"!sic07" {:gte "36000" :lt "41100"}}}}
                                                      {:value "sectionF" :label "Construction"
                                                       :filter {:range {"!sic07" {:gte "41100" :lt "45111"}}}}
                                                      {:value "sectionG" :label "Wholesale, retail & automative repair	"
                                                       :filter {:range {"!sic07" {:gte "45111" :lt "49100"}}}}
                                                      {:value "sectionH" :label "Transportation and storage"
                                                       :filter {:range {"!sic07" {:gte "49100" :lt "55100"}}}}
                                                      {:value "sectionI" :label "Accommodation, food & drink"
                                                       :filter {:range {"!sic07" {:gte "55100" :lt "58110"}}}}
                                                      #_{:value "sectionJ" :label "Information and communication"
                                                         :filter {:range {"!sic07" {:gte "58110" :lt "64110"}}}}
                                                      {:value "sectionJ1" :label "Publishing & Broadcasting"
                                                       :filter {:range {"!sic07" {:gte "58110" :lt "61100"}}}}
                                                      {:value "sectionJ2" :label "Telecommunications"
                                                       :filter {:range {"!sic07" {:gte "61100" :lt "62000"}}}}
                                                      {:value "sectionJ3" :label "IT: Software development"
                                                       :filter {:range {"!sic07" {:gte "62000" :lt "63120"}}}}
                                                      {:value "sectionJ4" :label "IT: Web & information services"
                                                       :filter {:range {"!sic07" {:gte "63120" :lt "64000"}}}}
                                                      {:value "sectionK" :label "Financial & insurance"
                                                       :filter {:range {"!sic07" {:gte "64110" :lt "68100" }}}}
                                                      {:value "sectionL" :label "Real estate"
                                                       :filter {:range {"!sic07" {:gte "68100" :lt "69101"}}}}
                                                      {:value "sectionM" :label "Scientific & technical services"
                                                       :filter {:range {"!sic07" {:gte "69101" :lt "77110"}}}}
                                                      {:value "sectionN" :label "Administrative & support services"
                                                       :filter {:range {"!sic07" {:gte "77110" :lt "84110"}}}}
                                                      {:value "sectionO" :label "Government & defence"
                                                       :filter {:range {"!sic07" {:gte "84110" :lt "85100"}}}}
                                                      {:value "sectionP" :label "Education"
                                                       :filter {:range {"!sic07" {:gte "85100" :lt "86101"}}}}
                                                      {:value "sectionQ" :label "Health & social work"
                                                       :filter {:range {"!sic07" {:gte "86101" :lt "90010"}}}}
                                                      {:value "sectionR" :label "Arts & entertainment"
                                                       :filter {:range {"!sic07" {:gte "90010" :lt "94110"}}}}
                                                      {:value "sectionS" :label "Other services"
                                                       :filter {:range {"!sic07" {:gte "94110" :lt "97000"}}}}
                                                      {:value "sectionT" :label "Household activities"
                                                       :filter {:range {"!sic07" {:gte "97000" :lt "99000" }}}}
                                                      {:value "sectionU" :label "International organisations"
                                                       :filter {:range {"!sic07" {:gte "99000"}}}}
                                                      ]}

                                           ;; {:id :ds
                                           ;;  :type :tag-checkboxes
                                           ;;  :label "Source"
                                           ;;  :visible true
                                           ;;  :controls true
                                           ;;  :tag-type "datasource"
                                           ;;  :tags [{:value "crunchbase" :label "Crunchbase"}
                                           ;;         {:value "angellist" :label "Angellist"}
                                           ;;         {:value "seeddb" :label "Seed-DB"}
                                           ;;         {:value "registry" :label "Registry"}]}
                                           ]

                         ;; base-filters AND combined with dynamic components
                         :composed {}}

   :dynamic-filter-description-components [:boundaryline :age :total-funding :sector :ds :hub :latest-turnover]

   :selection-filter-spec {:id :selection-filter
                           :components {:natural_id nil}
                           :base-filters {:all nil}
                           :composed {}}

   :company-search {:controls {:search-fn api/company-search
                               :render-fn (fn [r] [[:div (:name r)]
                                                   [:div (some->> (:tags r)
                                                                  (some (fn [t] (when (= "startup_region" (:type t)) t)))
                                                                  :description)]])
                               :col-headers ["Name" "Hub"]
                               :click-fn (fn [r]
                                           (make-company-selection (:natural_id r))
                                           (app/navigate @app-instance "company"))
                               }
                    :query nil
                    :results nil}

   :company-name {:path [:name]}

   :company-info {:controls {:index "companies"
                             :index-type "company"
                             :sort-spec nil
                             :size 1
                             :title-field :name}
                  :record nil}

   :edit-company-name {:path [:name]}

   :edit-company {:controls {:index "companies"
                             :index-type "company"
                             :sort-spec nil
                             :size 1
                             :title-field :name
                             :new-company nil}
                  :record nil}

   :map {:type :geoport
         :datasource "companies"
         ;; :boundaryline-collections [[0 "nuts_0"] [4 "nuts_1"] [6 "nuts_2"] [7 "nuts_3"] [8 "uk_boroughs"] [10 "uk_wards"]]
         ;; :boundaryline-collections [[0 "nuts_2"] [8 "nuts_3"] [9 "uk_boroughs"] [11 "uk_wards"]]
         :boundaryline-collections [[0 "nuts_2"] [8 "nuts_3"] [9 "nutsish_4"] [11 "nutsish_5"]]
         ;; :boundaryline-collections [[0 "uk_regions"] [5 "uk_counties"] [7 "uk_boroughs"] [10 "uk_wards"]]
         :controls {:initial-bounds [[71.0 35.0] [35.0 -11.0]]
                    :map-options {:zoomControl true
                                  :dragging true
                                  :touchZoom true
                                  :scrollWheelZoom false
                                  :doubleClickZoom true
                                  :boxZoom true
                                  }

                    :location {:cluster false
                               :location-attr "!location"
                               :attrs ["?natural_id" "!name" "!location" "!latest_employee_count" "!latest_turnover" "!total_funding"]
                               :sort-spec [{"!total_funding" "desc"}{"!latest_turnover" "desc"}]
                               :marker-opts {:display-funding true
                                             :display-turnover false
                                             :display-principal-name false}

                               :marker-render-fn (fn [location-sites location-spec]
                                                   (let [display-funding (get-in location-spec [:marker-opts :display-funding])
                                                         display-turnover (get-in location-spec [:marker-opts :display-turnover])
                                                         display-employment (get-in location-spec [:marker-opts :display-employment])
                                                         display-principal-name (get-in location-spec [:marker-opts :display-principal-name])]
                                                     (hiccups/html
                                                      [:div
                                                       (when (> (count location-sites) 1)
                                                         [:div [:p (num/compact (count location-sites) {:sf 2})]])
                                                       [:div.marker-info
                                                        (when display-principal-name
                                                          [:div.name
                                                           [:p
                                                            (-> location-sites first :name)]])
                                                        (when display-funding
                                                          [:div.minichart
                                                           [:div.minibar.metric-1
                                                            {:style (str "width: "
                                                                         (num/table-percent-scale
                                                                          [0 1000000 10000000]
                                                                          (->> location-sites (map :total_funding) (reduce +)))
                                                                         "%")}]])
                                                        (when display-turnover
                                                          [:div.minichart
                                                           [:div.minibar.metric-2
                                                            {:style (str "width: "
                                                                         (num/table-percent-scale
                                                                          [0 1000000 10000000]
                                                                          (->> location-sites (map :latest_turnover) (reduce +)))
                                                                         "%")}]])
                                                        (when display-employment
                                                          [:div.minichart
                                                           [:div.minibar.metric-3
                                                            {:style (str "width: "
                                                                         (num/table-percent-scale
                                                                          [0 100 1000]
                                                                          (->> location-sites (map :latest_employee_count) (reduce +)))
                                                                         "%")}]])]
                                                       ])))
                               :item-render-fn (fn [i]
                                                 [:div.item
                                                  [:div.name (get i :name)]
                                                  [:div.metrics
                                                   [:div.metric.metric-1
                                                    [:span.name "Fun"] [:span.value (num/compact (:total_funding i))]]
                                                   [:div.metric.metric-2
                                                    [:span.name "Rev"] [:span.value (num/compact (:latest_turnover i))]]
                                                   [:div.metric.metric-3
                                                    [:span.name "Emp"] [:span.value (num/compact (:latest_employee_count i))]]]])
                               :item-click-fn (fn [r e]
                                                (make-company-selection (:natural_id r))
                                                (app/navigate @app-instance "company")
                                                (.log js/console (clj->js ["CLICK" r e])))}

                    :zoom nil
                    :bounds nil
                    :show-points true
                    :display-company-links true
                    :boundaryline-collection nil
                    :boundaryline-agg {:type :stats
                                       :index "companies"
                                       :index-type "company"
                                       :key "boundaryline_id"
                                       :variable "!latest_employee_count"}
                    :colorchooser {:scheme [:RdPu :6]
                                   :scale :auto
                                   :variable :boundaryline_id_doc_count}

                    :boundaryline-fill-opacity 0.4

                    :geohash-aggs {:query {:index-name "companies"
                                           :index-type "company"
                                           :geo-point-field "!location"}
                                   :show-at-zoom-fn (fn [z] (>= z 8))
                                   :precision-fn (fn [z] (- (/ z 2) 0.5))
                                   :colorchooser-factory-fn (fn [geohash-aggs]
                                                              (let [chooser-fn (num/table-chooser-fn
                                                                                [0.7 0.9]
                                                                                (map :geohash-grid_doc_count geohash-aggs))]
                                                                (fn [geohash-agg]
                                                                  (chooser-fn (:geohash-grid_doc_count geohash-agg)))))
                                   :icon-render-fn (fn [geohash-agg]
                                                     [:p (num/compact (:geohash-grid_doc_count geohash-agg) {:sf 2})])
                                   :geohash-agg-data nil}

                    :geotag-aggs {:query {:index-name "companies"
                                          :index-type "company"
                                          :nested-path "?tags"
                                          :nested-attr "tag"
                                          :nested-filter {:term {:type "startup_region"}}
                                          :stats-attr "!total_funding"}
                                  :tag-type "startup_region"
                                  :show-at-zoom-fn (fn [z] (< z 8))
                                  :colorchooser-factory-fn (fn [geotag-aggs]
                                                             (let [chooser-fn (num/table-chooser-fn
                                                                               [0.7 0.9]
                                                                               (map :nested_attr_doc_count geotag-aggs))]
                                                               (fn ([geotag-agg]
                                                                    (chooser-fn :nested_attr_doc_count geotag-agg)))))
                                  :icon-render-fn (fn [tag stats]
                                                    [:p (num/compact (:nested_attr_doc_count stats) {:sf 2})])
                                  :click-fn (fn [geotag geotag-agg e]
                                              ;; (.log js/console (clj->js [(:description geotag) geotag geotag-agg e]))

                                              (let [boundaryline-id (:tag geotag)
                                                    ch (bl/get-or-fetch-boundaryline (get-app-state-atom) :boundarylines boundaryline-id)]
                                                (go
                                                  (let [bl (<! ch)
                                                        envelope (aget bl "envelope")
                                                        bounds (js->clj (map/postgis-envelope->latlngbounds envelope))]
                                                    (when bounds
                                                      (swap! (app/get-state @app-instance) assoc-in [:map :controls :bounds] bounds)

                                                      (make-boundaryline-selection boundaryline-id))))))

                                  :geotag-data nil
                                  :geotag-agg-data nil}

                    }
         :data nil}

   :headline-stats {:controls {:title nil
                               :fill-report-button []
                               :summary-stats {:index "companies"
                                               :index-type "company"
                                               :variables [{:key :?counter
                                                            :metric :viewfilter_doc_count
                                                            :label "Companies"
                                                            :render-fn (fn [v] (num/mixed v))}
                                                           {:key :!latest_turnover
                                                            :metric :sum
                                                            :label "Total revenue"
                                                            :render-fn (fn [v] (num/mixed v {:curr "€"}))}
                                                           {:key :!latest_employee_count
                                                            :metric :sum
                                                            :label "Total employees"
                                                            :render-fn (fn [v] (num/mixed v))}
                                                           {:key :!total_funding
                                                            :metric :sum
                                                            :label "Total funding"
                                                            :render-fn (fn [v] (num/mixed v {:curr "€"}))}
                                                           ]}}
                    :summary-stats nil
                    }

   :table  {:type :table
            :controls {:index "companies"
                       :index-type "company"
                       :sort-spec {:!total_funding {:order "desc"}}
                       :from 0
                       :size 50
                       :columns [
                                 {:key :!name :sortable true :label "Name" :render-fn company-link-render-fn}
                                 {:key :?tags :label "Hub" :render-fn (fn [tags] (->> tags (filter (fn [t] (= "startup_region" (:type t)))) first :description))}
                                 {:key :!formation_date :sortable true :label "Formation date" :render-fn #(time/format-date %)}
                                 ;; {:key :!latest_accounts_date :label "Filing date" :render-fn #(time/format-date %)}

                                 {:key :!latest_turnover :sortable true :label "Revenue" :render-fn #(num/mixed % )}
                                 {:key :!latest_turnover_delta
                                  :sortable true
                                  :label "Rev. change"
                                  :right-align true
                                  :render-fn (fn [v r]
                                               (let [pv (-! (:!latest_turnover r) v)
                                                     v (*! 100 (div! v pv))]
                                                 (when v
                                                   [:span
                                                    (num/mixed v) "%"
                                                    (sign-icon v)])))}

                                 {:key :!latest_employee_count :sortable true :label "Employees" :render-fn #(num/mixed %)}
                                 {:key :!latest_employee_count_delta
                                  :sortable true
                                  :label "Emp. change"
                                  :right-align true
                                  :render-fn (fn [v r]
                                               (let [pv (-! (:!latest_employee_count r) v)
                                                     v (*! 100 (div! v pv))]
                                                 (when v
                                                   [:span
                                                    (num/mixed v) "%"
                                                    (sign-icon v)])))}

                                 {:key :!total_funding :sortable true :label "Funding" :render-fn #(num/mixed %)}]}
            :table-data nil}

   :investment-timeline {:query {:index-name "company-funding-rounds"
                                 :index-type "funding-round"
                                 :time-variable "?raised_date"
                                 :metrics {:variable :!raised_amount_usd :title "Total investment (€)" :metric :sum}
                                 :interval "year"
                                 :before "2017-01-01"}
                         :color brand-secondary
                         :point-formatter (chart-helpers/mk-tooltip-point-formatter {})
                         :timeline-data nil}

   :employment-timeline {:query {:index-name "company-accounts"
                                 :index-type "accounts"
                                 :time-variable "?accounts_date"
                                 :metrics {:variable :!employee_count :title "Employees"}
                                 :interval "year"
                                 :before (time/today-str)
                                 }
                         :color brand-secondary
                         :timeline-data nil}

   :formation-timeline {:query {:index-name "companies"
                                :index-type "company"
                                :time-variable "!formation_date"
                                :metrics {:variable :?counter :metric :count :title "Formations"}
                                :interval "year"
                                ;; :before (time/today-str)
                                }
                        :color brand-secondary
                        :timeline-data nil}

   :company-turnover-timeline {:query {:index-name "company-funding-rounds"
                                       :index-type "funding-round"
                                       :time-variable "?raised_date"
                                       :metrics {:variable :!raised_amount_usd :title "Raised (€)"}
                                       :interval "year"
                                       :before (time/today-str)}
                               :color brand-secondary
                               :timeline-data nil}

   :company-employment-timeline {:query {:index-name "company-accounts"
                                         :index-type "accounts"
                                         :time-variable "?accounts_date"
                                         :metrics {:variable :!employee_count :title "Employees"}
                                         :interval "year"
                                         :before (time/today-str)}
                                 :color brand-secondary
                                 :timeline-data nil}

   :geo-sponsors {:controls {:max-count 1}
                  :data nil}

   :city-barchart {:query {:index-name "companies"
                           :index-type "company"
                           :nested-path "?tags"
                           :nested-attr "tag"
                           :nested-filter {:term {:type "startup_region"}}
                           :stats-attr "!total_funding"}
                   :metrics [{:metric :sum
                              :title "Total investment (€)"
                              :label-formatter (fn [] (this-as this (num/mixed (.-value this) {:sf 2})))}
                             ]
                   :bar-width 20
                   :bar-color brand-secondary
                   :point-formatter (chart-helpers/mk-tooltip-point-formatter {})
                   :yaxis-type "logarithmic"
                   :tag-type "startup_region"
                   :tag-data nil
                   :tag-agg-data nil}

   :sector-histogram {:query {:index-name "companies"
                              :index-type "company"
                              :nested-path "?tags"
                              :nested-attr "tag"
                              :nested-filter {:term {:type "ons_sector"}}
                              :stats-attr "?counter"}
                      :metrics [{:metric :nested_attr_doc_count
                                 :title "Companies"
                                 :label-formatter (fn [] (this-as this (num/mixed (.-value this) {:sf 2})))}]
                      :tag-type "ons_sector"
                      :tag-data nil
                      :tag-agg-data nil}

   :revenue-bands {:query {:index-name "companies"
                           :index-type "company"

                           :row-path [:accounts :row]
                           :row-aggs {:accounts
                                      {:nested {:path "?accounts"}
                                       :aggs
                                       {:row {:range {:field "rank"
                                                      :ranges [{:key "2014" :from 1 :to 2}]}}}} }

                           :col-path [:col]
                           :col-aggs {:col
                                      {:range {:field "turnover"
                                               :ranges [{:key "lt50k" :from 0       :to 50000 }
                                                        {:key "50k"   :from 50000   :to 100000 }
                                                        {:key "100k"  :from 100000  :to 250000 }
                                                        {:key "250k"  :from 250000  :to 500000}
                                                        {:key "500k"  :from 500000  :to 1000000}
                                                        {:key "1m"    :from 1000000 :to 5000000}
                                                        {:key "5m"    :from 5000000 }]}}
                                      :no-col-data {:missing {:field "turnover"}}}

                           :metric-path [:companies :metric]
                           :metric-aggs {:companies
                                         {:reverse_nested {}
                                          :aggs
                                          {:metric {:cardinality {:field "?natural_id"}}}}}
                           }
                   :view {:rows [{:key "2014" :label "2014"}]
                          :cols [{:key "lt50k"  :label "Less than €50k"}
                                 {:key "50k"  :label "€50k - €100k"}
                                 {:key "100k" :label "€100k - €250k"}
                                 {:key "250k"   :label "€250k - €500k"}
                                 {:key "500k"  :label "€500k - €1m"}
                                 {:key "1m"  :label "€1m - €5m"}
                                 {:key "5m" :label "More than €5m"}]
                          :color brand-secondary
                          :render-fn (fn [v] (num/mixed v))
                          }
                   :table-data nil}

   :employment-bands {:query {:index-name "companies"
                              :index-type "company"

                              :row-path [:accounts :row]
                              :row-aggs {:accounts
                                         {:nested {:path "?accounts"}
                                          :aggs
                                          {:row {:range {:field "rank"
                                                         :ranges [{:key "2014" :from 1 :to 2}]}}}} }

                              :col-path [:col]
                              :col-aggs {:col
                                         {:range {:field "employee_count"
                                                  :ranges [{:key "l"    :from 0    :to 5 }
                                                           {:key "5"    :from 5    :to 10 }
                                                           {:key "10"   :from 10   :to 20 }
                                                           {:key "20"   :from 20   :to 50 }
                                                           {:key "50"   :from 50   :to 100 }
                                                           {:key "100"  :from 100  :to 250 }
                                                           {:key "250"  :from 250  :to 500 }
                                                           {:key "500"  :from 500  :to 2500 }
                                                           {:key "2500" :from 2500 }] }}
                                         :no-col-data {:missing {:field "employee_count"}}}

                              :metric-path [:companies :metric]
                              :metric-aggs {:companies
                                            {:reverse_nested {}
                                             :aggs
                                             {:metric {:cardinality {:field "?natural_id"}}}}}
                              }
                      :view {:rows [{:key "2014" :label "2014"}]
                             :cols [{:key "l"    :label "1-4"}
                                    {:key "5"    :label "5-9"}
                                    {:key "10"   :label "10-19"}
                                    {:key "20"   :label "20-49"}
                                    {:key "50"   :label "50-99"}
                                    {:key "100"  :label "100-249"}
                                    {:key "250"  :label "250-499"}
                                    {:key "500"  :label "500-2499"}
                                    {:key "2500" :label "2500 or more"}]
                             :color brand-secondary
                             :render-fn (fn [v] (num/fnum v))
                             }
                      :table-data nil}


   :view :trends

   :new-company-edit {:content (constantly "Add company")
                      :action edit-new-company-fn}

   :company-edit {:text "Edit"
                  :class "btn btn-primary"
                  :action edit-company-fn}

   :company-close {:text "Close"
                   :target-view "main"
                   :class "btn btn-primary"}

   :edit-company-close {:text "Close"
                        :target-view "main"
                        :class "btn btn-primary"}

   :reset-map-view {:text "Reset view"
                    :action (fn [e]
                              (swap! (get-app-state-atom)
                                     assoc-in
                                     [:map :controls :bounds]
                                     (get-in @(get-app-state-atom) [:map :controls :initial-bounds])))
                    :class "btn btn-default"}

   :reset-all {:content (constantly [:h1.logo "EC"])
               :action (fn [e]
                         (js/console.log "reset all")
                         (some-> e .preventDefault)

                         (reset! (get-app-state-atom)
                                 (-> @(get-app-state-atom)
                                     (assoc-in [:map :controls :bounds]
                                               (get-in @(get-app-state-atom) [:map :controls :initial-bounds]))
                                     (assoc-in [:dynamic-filter-spec]
                                               (filters/reset-filter (get-in @(get-app-state-atom) [:dynamic-filter-spec]))))))}
   })


(def components
  [
   {:name :filter
    :f filter/filter-component
    :target "filter-component"
    :paths {:filter-spec [:dynamic-filter-spec]}}

   {:name :company-search
    :f search/search-component
    :target "company-search-component"
    :paths {:search [:company-search]}}

   {:name :filter-description
    :f filter-description/filter-description-component
    :target "filter-description-component"
    :paths {:components [:dynamic-filter-description-components]
            :filter-spec [:dynamic-filter-spec]}}

   {:name :headline-stats
    :f map-report/map-report-component
    :target "headline-stats-component"
    :paths {:map-report [:headline-stats]
            :filter [:dynamic-filter-spec :composed :all]}}

   {:name :map
    :f map/map-component
    :target "map-component"
    :paths {:map-state [:map]
            :filter [:dynamic-filter-spec :composed :all]}}

   {:name :color-scale
    :f color-scale/color-scale-component
    :target "color-scale-component"
    :path [:map :controls :threshold-colors]}

   {:name :new-company-edit
    :f action-link/action-link-component
    :target "new-company-edit"
    :paths {:action-link [:new-company-edit]}}

   {:name :company-edit
    :f action-button/action-button-component
    :target "company-edit"
    :paths {:action-button [:company-edit]}}

   {:name :company-close
    :f nav-button/nav-button-component
    :target "company-close"
    :paths {:nav-button [:company-close]}}

   ;; {:name :edit-company-close
   ;;  :f nav-button/nav-button-component
   ;;  :target "edit-company-close"
   ;;  :paths {:nav-button [:edit-company-close]}}

   {:name :reset-map-view
    :f action-button/action-button-component
    :target "reset-map-view-button-component"
    :paths {:action-button [:reset-map-view]}}

   {:name :reset-all
    :f action-link/action-link-component
    :target "reset-all-component"
    :paths {:action-link [:reset-all]}}

   {:name :display-minichart-turnover
    :f (partial checkbox-boolean/checkbox-boolean-component :display-turnover)
    :target "display-minichart-turnover-component"
    :path [:map :controls :location :marker-opts]}

   {:name :display-minichart-employment
    :f (partial checkbox-boolean/checkbox-boolean-component :display-employment)
    :target "display-minichart-employment-component"
    :path [:map :controls :location :marker-opts]}

   {:name :display-minichart-funding
    :f (partial checkbox-boolean/checkbox-boolean-component :display-funding)
    :target "display-minichart-funding-component"
    :path [:map :controls :location :marker-opts]}

   {:name :display-principal-name
    :f (partial checkbox-boolean/checkbox-boolean-component :display-principal-name)
    :target "display-principal-name-component"
    :path [:map :controls :location :marker-opts]}

   {:name :display-company-links
    :f (partial checkbox-boolean/checkbox-boolean-component :display-company-links)
    :target "display-company-links-component"
    :path [:map :controls]}

   {:name :city-barchart-var-select
    :f (partial
        select-chooser/select-chooser-component
        "Variable"
        [{:value "!total_funding" :label "Total investment (€)"}
         {:value "!latest_employee_count" :label "Employee count"}
         {:value "!latest_turnover" :label "Revenue (€)"}
         {:value "?counter" :label "No. of companies"}]
        (fn
          ([cursor] (get-in cursor [:query :stats-attr]))
          ([cursor record]
           (om/update! cursor [:query :stats-attr] (:value record))
           (om/update! cursor [:metrics 0 :title] (:label record)))))
    :target "city-barchart-var-select-component"
    :path [:city-barchart]}

   {:name :region-investment-histogram
    :f tag-histogram/tag-histogram
    :target "city-barchart-component"
    :paths {:tag-histogram [:city-barchart]
            :filter-spec [:dynamic-filter-spec :composed :all]}}

   {:name :sector-histogram
    :f tag-histogram/tag-histogram
    :target "sector-histogram"
    :paths {:tag-histogram [:sector-histogram]
            :filter-spec [:dynamic-filter-spec :composed :all]}}

   {:name :table
    :f table/table-component
    :target "table-component"
    :paths {:table-state [:table]
            :filter-spec [:dynamic-filter-spec :composed :all]}}

   {:name :investment-timeline
    :f timeline-chart/timeline-chart
    :target "investment-timeline-component"
    :paths {:timeline-chart [:investment-timeline]
            :filter-spec [:dynamic-filter-spec :composed :all]}}

   {:name :investment-timeline-var-select
    :f (partial select-chooser/select-chooser-component
                "Variable"
                [{:value :sum :label "Total investment (€)"}
                 {:value :count :label "Number of deals"}]
                (fn
                  ([cursor] (get-in cursor [:query :metrics :metric]))
                  ([cursor record]
                   (om/update! cursor [:query :metrics :metric] (:value record))
                   (om/update! cursor [:query :metrics :title] (:label record))
                   )))
    :target "investment-timeline-var-select-component"
    :path [:investment-timeline]}

   ;; {:name :employment-timeline
   ;;  :f timeline-chart/timeline-chart
   ;;  :target "employment-timeline"
   ;;  :paths {:timeline-chart [:employment-timeline]
   ;;          :filter-spec [:dynamic-filter-spec :composed :all]}}

   ;; {:name :formation-timeline
   ;;  :f timeline-chart/timeline-chart
   ;;  :target "formation-timeline"
   ;;  :paths {:timeline-chart [:formation-timeline]
   ;;          :filter-spec [:dynamic-filter-spec :composed :all]}}

   ;; {:name :geo-sponsors
   ;;  :f geo-sponsors/geo-sponsors-component
   ;;  :target "geo-sponsors"
   ;;  :paths {:bounds [:map :controls :bounds]
   ;;          :geo-sponsors [:geo-sponsors]}}

   ;; {:name :revenue-bands-var-select
   ;;  :f (partial select-chooser/select-chooser-component "Variable" :field [[:turnover "Revenue (€)"][:employee_count "Employment"]])
   ;;  :target "revenue-bands-var-select-component"
   ;;  :path [:revenue-bands-table :controls :col-aggs :col :range]}

   {:name :revenue-bands-chart
    :f ranges-chart/ranges-chart-component
    :target "revenue-bands-chart-component"
    :paths {:table-state [:revenue-bands]
            :filter-spec [:dynamic-filter-spec :composed :all]}}

   {:name :employment-bands-chart
    :f ranges-chart/ranges-chart-component
    :target "employment-bands-chart-component"
    :paths {:table-state [:employment-bands]
            :filter-spec [:dynamic-filter-spec :composed :all]}}

   ;; {:name :company-turnover-timeline
   ;;  :f timeline-chart/timeline-chart
   ;;  :target "company-turnover-timeline"
   ;;  :paths {:timeline-chart [:company-turnover-timeline]
   ;;          :filter-spec [:selection-filter-spec :composed :all]}}

   ;; {:name :company-employment-timeline
   ;;  :f timeline-chart/timeline-chart
   ;;  :target "company-employment-timeline"
   ;;  :paths {:timeline-chart [:company-employment-timeline]
   ;;          :filter-spec [:selection-filter-spec :composed :all]}}

   {:name :company-name
    :f text/text-component
    :target "company-name-component"
    :paths {:source [:company-info :record]
            :controls [:company-name]}}

   {:name :company-info
    :f company-info/company-info-component
    :target "company-info-component"
    :paths {:metadata [:company-info]
            :filter-spec [:selection-filter-spec :composed :all]}}

   {:name :edit-company-name
    :f text/text-component
    :target "edit-company-name-component"
    :paths {:source [:edit-company :record]
            :controls [:edit-company-name]}}

   {:name :edit-company
    :f edit-company/edit-company-component
    :target "edit-company-component"
    :paths {:metadata [:edit-company]
            :filter-spec [:selection-filter-spec :composed :all]}}

   ]
  )

;;;;;;;;;;;;;;;;;;;;;;;; load and index boundarylines

(def bl-collections ["nuts_2"])

(defn load-boundaryline-collection-indexes
  [state]
  (doseq [blcoll bl-collections]
    (bl/fetch-boundaryline-collection-index state :boundarylines blcoll)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn create-app-service
  []
  (let [event-handlers (atom nil)]
    (reify

      app/IAppService

      (init [this app]
        (load-boundaryline-collection-indexes (app/get-state app))
        {:fetch-boundarylines-fn (partial bl/get-or-fetch-best-boundarylines (app/get-state app) :boundarylines)
         :get-cached-boundaryline-fn (partial bl/get-cached-boundaryline (app/get-state app) :boundarylines)
         :point-in-boundarylines-fn (partial bl/point-in-boundarylines (app/get-state app) :boundarylines :uk_boroughs)
         :path-marker-click-fn make-boundaryline-selection
         :submit-company-fn #(api/POST (str "/api/" api/api-prefix "/eustartuphubs/submit-company")
                                       % :send-error true)
         :fetch-metadata-factory api/records-factory
         :navigate-fn (fn [slug] (app/navigate @clustermap.core/app-instance slug))})

      (destroy [this app]
        (.log js/console "DESTROY APP!"))

      (handle-event [this app type val]
        )

      (dev-mode? [this] dev-mode))))

(defn init
  "Only start when full app (not devcards)"
  []
  (if (.getElementById js/document "intro")
    (app/start-or-restart-app app-instance initial-state components (create-app-service))))


(init)

;; (cond

;;   ;; dev mode : configure repl and figwheel code-reloading
;;   js/config.repl
;;   (do
;;     (ws-repl/connect "ws://localhost:9001" :verbose true)
;;     (fw/watch-and-reload
;;      :websocket-url "ws://localhost:3449/figwheel-ws"
;;      :jsload-callback (fn []
;;                         (init)
;;                         (.log js/console "restarted")))
;;     (init))

;;   ;; production : just run the app
;;   true
;;   (init))
