(defproject clustermap-eustartuphubs-cljs "0.1.0-SNAPSHOT"
  :description "FIXME: write this!"
  :url "http://example.com/FIXME"

  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/clojurescript "1.8.40" :scope "provided"]
                 [org.clojure/core.async "0.2.374" ]
                 [domina "1.0.3"]
                 [jayq "2.5.4"]
                 [org.omcljs/om "1.0.0-alpha26"]
                 [prismatic/schema "1.1.0"]
                 [sablono "0.7.0"]
                 [cljsjs/react "0.14.3-0"]
                 [cljsjs/react-dom "0.14.3-1"]
                 ;; [cljsjs/react-dom-server "0.14.3-0"]
                 [hiccups "0.3.0"]
                 [secretary "1.2.3"]
                 [com.andrewmcveigh/cljs-time "0.4.0"]
                 [binaryage/devtools "0.4.1"]
                 [clustermap-components "0.1.0-SNAPSHOT"]
                 ]

  :source-paths ["src"]
  ;;:clean-targets ^{:protect false} ["cljs-out/"]

  :plugins [[lein-cljsbuild "1.1.2"]
            [lein-figwheel "0.5.0-6"]]

  :profiles {:dev {:dependencies [[weasel "0.7.0"]
                                  [com.cemerick/piggieback "0.2.1"]
                                  [devcards "0.2.1-6"]
                                  [figwheel-sidecar "0.5.2"]
                                  [figwheel "0.5.2"]]}}

  :repl-options {:nrepl-middleware [cemerick.piggieback/wrap-cljs-repl]}
  :figwheel {:css-dirs ["public/css"]}
  :cljsbuild {
              :builds [{:id "none"
                        :source-paths ["src"
                                       "checkouts/clustermap-components/src"]
                        :figwheel {:on-jsload "clustermap.core/init"
                                   ;;:load-warninged-code true
                                   }
                        ;;:devcards true
                        :compiler {
                                   ;; resources/public path necessary for figwheel, so frigged
                                   ;; with softlink from resources/public to cljs-out/none
                                   :output-to  "resources/public/clustermap.js"
                                   :source-map true
                                   :main "clustermap.core"
                                   :output-dir "resources/public/cljs"
                                   :optimizations :none
                                   :pretty-print true}}

                       {:id "devcards"
                        :source-paths ["src"
                                       "checkouts/clustermap-components/src"]
                        :figwheel {:devcards true}
                        :compiler {
                                   :output-to "resources/public/devcards.js"
                                   :source-map true
                                   :main "clustermap.devcards"
                                   :output-dir "resources/public/cljs"
                                   :optimizations :none
                                   :pretty-print true}}

                       {:id :whitespace
                        :source-paths ["src"]
                        :compiler {
                                   :output-to  "cljs-out/whitespace/clustermap.js"
                                   :source-map "cljs-out/whitespace/clustermap.js.map"
                                   :output-dir "cljs-out/whitespace/cljs"
                                   :optimizations :whitespace
                                   :pretty-print true
                                   :output-wrapper false}}

                       {:id "simple"
                        :source-paths ["src"]
                        :compiler {
                                   :output-to  "cljs-out/simple/clustermap.js"
                                   :source-map "cljs-out/simple/clustermap.js.map"
                                   :output-dir "cljs-out/simple/cljs"
                                   :optimizations :simple
                                   :main "clustermap.core"
                                   :pretty-print true
                                   :output-wrapper false}}

                       {:id "advanced"
                        :source-paths ["src"]
                        :compiler {
                                   :output-to  "cljs-out/advanced/clustermap.js"
                                   :source-map "cljs-out/advanced/clustermap.js.map"
                                   :output-dir "cljs-out/advanced/cljs"
                                   :optimizations :advanced
                                   :main "clustermap.core"
                                   :output-wrapper false
                                   :externs ["react/externs/react.js"]}}]})
